incremental>=16.9.0
python-dateutil

[:python_version<'3']
Twisted[tls]>=15.5.0

[:python_version>'3']
Twisted[tls]>=16.4.0

[dev]
pyflakes
coverage
sphinx
towncrier

[dev:python_version<'3']
pydoctor
