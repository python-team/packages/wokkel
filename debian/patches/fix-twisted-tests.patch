From: Colin Watson <cjwatson@debian.org>
Date: Sun, 8 Dec 2024 16:21:02 +0000
Subject: Fix tests with current Twisted versions

This effectively drops Python 2 support.

Last-Update: 2024-12-08
---
 setup.py                      |  1 +
 wokkel/data_form.py           | 20 ++++++++++++--------
 wokkel/disco.py               |  4 ++--
 wokkel/muc.py                 |  3 ++-
 wokkel/pubsub.py              |  6 +++---
 wokkel/server.py              |  4 ++--
 wokkel/subprotocols.py        |  5 ++---
 wokkel/test/helpers.py        |  3 +--
 wokkel/test/test_client.py    |  2 +-
 wokkel/test/test_data_form.py |  8 ++++----
 wokkel/test/test_muc.py       |  4 ++--
 wokkel/xmppim.py              | 10 +++++-----
 12 files changed, 37 insertions(+), 33 deletions(-)

diff --git a/setup.py b/setup.py
index 8804fd9..965d156 100755
--- a/setup.py
+++ b/setup.py
@@ -58,6 +58,7 @@ setup(name='wokkel',
       ],
       use_incremental=True,
       install_requires=[
+          'constantly',
           'incremental>=16.9.0',
           'python-dateutil',
       ],
diff --git a/wokkel/data_form.py b/wokkel/data_form.py
index ed9c5fc..7b66d10 100644
--- a/wokkel/data_form.py
+++ b/wokkel/data_form.py
@@ -14,13 +14,17 @@ U{XEP-0068<http://xmpp.org/extensions/xep-0068.html>}.
 
 from __future__ import division, absolute_import
 
+import sys
+
 from zope.interface import implementer
 from zope.interface.common import mapping
 
-from twisted.python.compat import iteritems, unicode, _PY3
+from twisted.python.compat import unicode
 from twisted.words.protocols.jabber.jid import JID
 from twisted.words.xish import domish
 
+_PY3 = sys.version_info >= (3, 0)
+
 NS_X_DATA = 'jabber:x:data'
 
 
@@ -147,7 +151,7 @@ class Field(object):
         try:
             self.options = [Option(optionValue, optionLabel)
                             for optionValue, optionLabel
-                            in iteritems(options)]
+                            in options.items()]
         except AttributeError:
             self.options = options or []
 
@@ -313,9 +317,9 @@ class Field(object):
     def fromElement(element):
         field = Field(None)
 
-        for eAttr, fAttr in iteritems({'type': 'fieldType',
-                                       'var': 'var',
-                                       'label': 'label'}):
+        for eAttr, fAttr in {'type': 'fieldType',
+                             'var': 'var',
+                             'label': 'label'}.items():
             value = element.getAttribute(eAttr)
             if value:
                 setattr(field, fAttr, value)
@@ -350,7 +354,7 @@ class Field(object):
 
         if 'options' in fieldDict:
             options = []
-            for value, label in iteritems(fieldDict['options']):
+            for value, label in fieldDict['options'].items():
                 options.append(Option(value, label))
             kwargs['options'] = options
 
@@ -497,7 +501,7 @@ class Form(object):
             C{fieldDefs}.
         @type filterUnknown: L{bool}
         """
-        for name, value in iteritems(values):
+        for name, value in values.items():
             fieldDict = {'var': name,
                          'type': None}
 
@@ -701,7 +705,7 @@ class Form(object):
 
         filtered = []
 
-        for name, field in iteritems(self.fields):
+        for name, field in self.fields.items():
             if name in fieldDefs:
                 fieldDef = fieldDefs[name]
                 if 'type' not in fieldDef:
diff --git a/wokkel/disco.py b/wokkel/disco.py
index 9ea43ef..f49217b 100644
--- a/wokkel/disco.py
+++ b/wokkel/disco.py
@@ -13,7 +13,7 @@ U{XEP-0030<http://xmpp.org/extensions/xep-0030.html>}.
 from __future__ import division, absolute_import
 
 from twisted.internet import defer
-from twisted.python.compat import iteritems, unicode
+from twisted.python.compat import unicode
 from twisted.words.protocols.jabber import error, jid
 from twisted.words.xish import domish
 
@@ -366,7 +366,7 @@ class _DiscoRequest(generic.Request):
             NS_DISCO_ITEMS: 'items',
             }
 
-    _verbRequestMap = dict(((v, k) for k, v in iteritems(_requestVerbMap)))
+    _verbRequestMap = dict(((v, k) for k, v in _requestVerbMap.items()))
 
     def __init__(self, verb=None, nodeIdentifier='',
                        recipient=None, sender=None):
diff --git a/wokkel/muc.py b/wokkel/muc.py
index 330664b..9d5a44e 100644
--- a/wokkel/muc.py
+++ b/wokkel/muc.py
@@ -14,11 +14,12 @@ from __future__ import division, absolute_import
 
 from dateutil.tz import tzutc
 
+from constantly import Values, ValueConstant
+
 from zope.interface import implementer
 
 from twisted.internet import defer
 from twisted.python.compat import unicode
-from twisted.python.constants import Values, ValueConstant
 from twisted.words.protocols.jabber import jid, error, xmlstream
 from twisted.words.xish import domish
 
diff --git a/wokkel/pubsub.py b/wokkel/pubsub.py
index 689a6e2..f0c80e2 100644
--- a/wokkel/pubsub.py
+++ b/wokkel/pubsub.py
@@ -16,7 +16,7 @@ from zope.interface import implementer
 
 from twisted.internet import defer
 from twisted.python import log
-from twisted.python.compat import StringType, iteritems, unicode
+from twisted.python.compat import StringType, unicode
 from twisted.words.protocols.jabber import jid, error
 from twisted.words.xish import domish
 
@@ -277,7 +277,7 @@ class PubSubRequest(generic.Stanza):
     }
 
     # Map request verb to request iq type and subelement name
-    _verbRequestMap = dict(((v, k) for k, v in iteritems(_requestVerbMap)))
+    _verbRequestMap = dict(((v, k) for k, v in _requestVerbMap.items()))
 
     # Map request verb to parameter handler names
     _parameters = {
@@ -1356,7 +1356,7 @@ class PubSubService(XMPPHandler, IQHandlerMixin):
         if request.nodeIdentifier:
             affiliations['node'] = request.nodeIdentifier
 
-        for entity, affiliation in iteritems(result):
+        for entity, affiliation in result.items():
             item = affiliations.addElement('affiliation')
             item['jid'] = entity.full()
             item['affiliation'] = affiliation
diff --git a/wokkel/server.py b/wokkel/server.py
index 54517a2..6d12df4 100644
--- a/wokkel/server.py
+++ b/wokkel/server.py
@@ -22,7 +22,7 @@ from zope.interface import implementer
 from twisted.internet import defer, reactor
 from twisted.names.srvconnect import SRVConnector
 from twisted.python import log, randbytes
-from twisted.python.compat import iteritems, unicode
+from twisted.python.compat import unicode
 from twisted.words.protocols.jabber import error, ijabber, jid, xmlstream
 from twisted.words.xish import domish
 
@@ -340,7 +340,7 @@ class XMPPServerListenAuthenticator(xmlstream.ListenAuthenticator):
         try:
             if xmlstream.NS_STREAMS != rootElement.uri or \
                self.namespace != self.xmlstream.namespace or \
-               ('db', NS_DIALBACK) not in iteritems(rootElement.localPrefixes):
+               ('db', NS_DIALBACK) not in rootElement.localPrefixes.items():
                 raise error.StreamError('invalid-namespace')
 
             if targetDomain and targetDomain not in self.service.domains:
diff --git a/wokkel/subprotocols.py b/wokkel/subprotocols.py
index f0a6090..7043d1c 100644
--- a/wokkel/subprotocols.py
+++ b/wokkel/subprotocols.py
@@ -14,7 +14,6 @@ from zope.interface import implementer
 from twisted.internet import defer
 from twisted.internet.error import ConnectionDone
 from twisted.python import failure, log
-from twisted.python.compat import iteritems, itervalues
 from twisted.python.deprecate import deprecatedModuleAttribute
 from twisted.words.protocols.jabber import error, ijabber, xmlstream
 from twisted.words.protocols.jabber.xmlstream import toResponse
@@ -277,7 +276,7 @@ class StreamManager(XMPPHandlerCollection):
         # deferreds will never be fired.
         iqDeferreds = self._iqDeferreds
         self._iqDeferreds = {}
-        for d in itervalues(iqDeferreds):
+        for d in iqDeferreds.values():
             d.errback(reason)
 
 
@@ -455,7 +454,7 @@ class IQHandlerMixin(object):
             return error.StanzaError('internal-server-error').toResponse(iq)
 
         handler = None
-        for queryString, method in iteritems(self.iqHandlers):
+        for queryString, method in self.iqHandlers.items():
             if xpath.internQuery(queryString).matches(iq):
                 handler = getattr(self, method)
 
diff --git a/wokkel/test/helpers.py b/wokkel/test/helpers.py
index 102b3dc..76fc757 100644
--- a/wokkel/test/helpers.py
+++ b/wokkel/test/helpers.py
@@ -8,7 +8,6 @@ Unit test helpers.
 from __future__ import division, absolute_import
 
 from twisted.internet import defer
-from twisted.python.compat import iteritems
 from twisted.words.xish import xpath
 from twisted.words.xish.utility import EventDispatcher
 
@@ -86,7 +85,7 @@ class TestableRequestHandlerMixin(object):
         """
         handler = None
         iq = parseXml(xml)
-        for queryString, method in iteritems(self.service.iqHandlers):
+        for queryString, method in self.service.iqHandlers.items():
             if xpath.internQuery(queryString).matches(iq):
                 handler = getattr(self.service, method)
 
diff --git a/wokkel/test/test_client.py b/wokkel/test/test_client.py
index ef367f7..e2e8812 100644
--- a/wokkel/test/test_client.py
+++ b/wokkel/test/test_client.py
@@ -152,7 +152,7 @@ class ClientCreatorTest(unittest.TestCase):
 
         def cb(connector):
             self.assertEqual('xmpp-client', connector.service)
-            self.assertEqual('example.org', connector.domain)
+            self.assertEqual(b'example.org', connector.domain)
             self.assertEqual(factory, connector.factory)
 
         def connect(connector):
diff --git a/wokkel/test/test_data_form.py b/wokkel/test/test_data_form.py
index 60e36f4..fcd5251 100644
--- a/wokkel/test/test_data_form.py
+++ b/wokkel/test/test_data_form.py
@@ -10,7 +10,7 @@ from __future__ import division, absolute_import
 from zope.interface import verify
 from zope.interface.common.mapping import IIterableMapping
 
-from twisted.python.compat import unicode, _PY3
+from twisted.python.compat import unicode
 from twisted.trial import unittest
 from twisted.words.xish import domish
 from twisted.words.protocols.jabber import jid
@@ -1137,7 +1137,7 @@ class FormTest(unittest.TestCase):
                                                 values=['news', 'search'])]
         form = data_form.Form('submit', fields=fields)
         keys = form.keys()
-        if not _PY3:
+        if not data_form._PY3:
             self.assertIsInstance(keys, list)
         self.assertEqual(set(['botname', 'public', 'features']),
                          set(keys))
@@ -1151,7 +1151,7 @@ class FormTest(unittest.TestCase):
                   data_form.Field('boolean', var='public', value=True)]
         form = data_form.Form('submit', fields=fields)
         values = form.values()
-        if not _PY3:
+        if not data_form._PY3:
             self.assertIsInstance(values, list)
         self.assertEqual(set(['The Jabber Bot', True]), set(values))
 
@@ -1164,7 +1164,7 @@ class FormTest(unittest.TestCase):
                   data_form.Field('boolean', var='public', value=True)]
         form = data_form.Form('submit', fields=fields)
         items = form.items()
-        if not _PY3:
+        if not data_form._PY3:
             self.assertIsInstance(items, list)
         self.assertEqual(set([('botname', 'The Jabber Bot'),
                               ('public', True)]),
diff --git a/wokkel/test/test_muc.py b/wokkel/test/test_muc.py
index f690d05..699daa3 100644
--- a/wokkel/test/test_muc.py
+++ b/wokkel/test/test_muc.py
@@ -14,7 +14,7 @@ from zope.interface import verify
 
 from twisted.trial import unittest
 from twisted.internet import defer, task
-from twisted.python.compat import iteritems, unicode
+from twisted.python.compat import unicode
 from twisted.words.xish import domish, xpath
 from twisted.words.protocols.jabber.jid import JID
 from twisted.words.protocols.jabber.error import StanzaError
@@ -81,7 +81,7 @@ class StatusCodeTest(unittest.TestCase):
             332: 'removed-shutdown',
         }
 
-        for code, condition in iteritems(codes):
+        for code, condition in codes.items():
             constantName = condition.replace('-', '_').upper()
             self.assertEqual(getattr(muc.STATUS_CODE, constantName),
                              muc.STATUS_CODE.lookupByValue(code))
diff --git a/wokkel/xmppim.py b/wokkel/xmppim.py
index e6af929..a45347f 100644
--- a/wokkel/xmppim.py
+++ b/wokkel/xmppim.py
@@ -15,7 +15,7 @@ from __future__ import division, absolute_import
 import warnings
 
 from twisted.internet import defer
-from twisted.python.compat import iteritems, itervalues, unicode
+from twisted.python.compat import unicode
 from twisted.words.protocols.jabber import error
 from twisted.words.protocols.jabber.jid import JID
 from twisted.words.xish import domish
@@ -48,7 +48,7 @@ class AvailablePresence(Presence):
             self.addElement('show', content=show)
 
         if statuses is not None:
-            for lang, status in iteritems(statuses):
+            for lang, status in statuses.items():
                 s = self.addElement('status', content=status)
                 if lang:
                     s[(NS_XML, "lang")] = lang
@@ -61,7 +61,7 @@ class UnavailablePresence(Presence):
         Presence.__init__(self, to, type='unavailable')
 
         if statuses is not None:
-            for lang, status in iteritems(statuses):
+            for lang, status in statuses.items():
                 s = self.addElement('status', content=status)
                 if lang:
                     s[(NS_XML, "lang")] = lang
@@ -309,7 +309,7 @@ class AvailabilityPresence(BasePresence):
         if None in self.statuses:
             return self.statuses[None]
         elif self.statuses:
-            for status in itervalues(self.status):
+            for status in self.status.values():
                 return status
         else:
             return None
@@ -355,7 +355,7 @@ class AvailabilityPresence(BasePresence):
             if self.priority != 0:
                 presence.addElement('priority', content=unicode(self.priority))
 
-        for lang, text in iteritems(self.statuses):
+        for lang, text in self.statuses.items():
             status = presence.addElement('status', content=text)
             if lang:
                 status[(NS_XML, 'lang')] = lang
